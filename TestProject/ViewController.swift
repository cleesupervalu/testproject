//
//  ViewController.swift
//  TestProject
//
//  Created by Charles Lee on 8/9/18.
//  Copyright © 2018 Charles Lee. All rights reserved.
//  Change 3
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let paging = UIPageControl()
    
    let frame = UIScreen.main.bounds
    
    let imgName = ["1", "2", "3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        setCollectionView()
        
        paging.frame = CGRect(x: (collectionView.frame.width/2) - 50, y: collectionView.frame.height + 20, width: 100, height: 20)

        paging.numberOfPages = self.imgName.count
        paging.currentPageIndicatorTintColor = UIColor.green
        paging.pageIndicatorTintColor = UIColor.black
        view.addSubview(paging)
        
        let btnNavigate = UIButton()
        btnNavigate.frame = CGRect(x: (collectionView.frame.width/2) - 50, y: paging.frame.origin.y + 100, width: 100, height: 50)
        btnNavigate.setTitle("Navigate", for: .normal)
        btnNavigate.backgroundColor = UIColor.black
        btnNavigate.addTarget(self, action: #selector(btnPressed), for: .touchDown)
        
        view.addSubview(btnNavigate)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "resultSegue"{
            let v = segue.destination as! ResultViewController
            v.selectedIndex = self.paging.currentPage + 1
        }
    }
    
    @objc func btnPressed(){
        performSegue(withIdentifier: "resultSegue", sender: nil)
    }
    
    func setCollectionView(){
        let cellWidth = frame.width
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
            flowLayout.itemSize = CGSize(width: cellWidth, height: cellWidth)
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.paging.currentPage = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.backgroundColor = UIColor.yellow
        cell.img.image = UIImage(named: imgName[indexPath.row])
        
        return cell
    }


}

