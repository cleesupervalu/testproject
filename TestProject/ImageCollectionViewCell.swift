//
//  ImageCollectionViewCell.swift
//  TestProject
//
//  Created by Charles Lee on 8/9/18.
//  Copyright © 2018 Charles Lee. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!

}
