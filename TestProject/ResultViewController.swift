//
//  ResultViewController.swift
//  TestProject
//
//  Created by Charles Lee on 8/9/18.
//  Copyright © 2018 Charles Lee. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    var selectedIndex: Int?
    @IBOutlet weak var lblResult: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        if let inx = selectedIndex{
            self.lblResult.text = "Selected index is : " + inx.description
        }
        
        let viewTapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTouched))
        view.addGestureRecognizer(viewTapGesture)
    }
    
    @objc func viewTouched(){
        dismiss(animated: true, completion: nil)
    }


}
